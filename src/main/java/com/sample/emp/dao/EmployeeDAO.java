package com.sample.emp.dao;

import org.springframework.beans.factory.annotation.Autowired;

import com.sample.emp.repository.EmployeeRepository;
import com.sample.emp.model.Employee;
import java.util.List;
import java.util.Optional;
import org.springframework.stereotype.Service;

@Service
public class EmployeeDAO {
	
	@Autowired
	EmployeeRepository employeeRepository;
	private List<Employee> employees;
	private Integer count;

	public Employee save(Employee emp) {
		return employeeRepository.save(emp);
	}

	public List<Employee> getEmployees() {
		return employees;
	}

	public void setEmployees(List<Employee> employees) {
		this.employees = employees;
	}

	public List<Employee> findAll() {
		return employeeRepository.findAll();
	}

	/* get an employee by id */
	public Optional<Employee> findById(Long empid) {
		return employeeRepository.findById(empid);
	}

	public void delete(Long empid) {
		employeeRepository.deleteById(empid);
	}

	public Integer countById() {
		return (int) employeeRepository.count();
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

}
