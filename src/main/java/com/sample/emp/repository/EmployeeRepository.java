package com.sample.emp.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;

import com.sample.emp.model.Employee;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@SuppressWarnings("unused")
public interface EmployeeRepository extends JpaRepository<Employee, Long> {

	Optional<Employee> findById(Long Id);

	void deleteById(Long emp);

	long count();

	Employee save(List<Employee> emp);

	List<Employee> findAll();

}
