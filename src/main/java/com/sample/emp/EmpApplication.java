package com.sample.emp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.sample.emp.EmpApplication;

@SpringBootApplication
public class EmpApplication {
	public static void main(String[] args) {
 		SpringApplication.run(EmpApplication.class, args);
	}

}
