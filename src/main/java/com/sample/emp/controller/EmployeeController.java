package com.sample.emp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import java.util.Optional;
import javax.validation.Valid;
import com.sample.emp.model.Employee;
import com.sample.emp.dao.EmployeeDAO;

@RestController
@RequestMapping("/company")
public class EmployeeController {

	@Autowired
	EmployeeDAO employeeDAO;

	@GetMapping("/employees")
	public ResponseEntity<EmployeeDAO> getAllEmployees() {
		List<Employee> employees = employeeDAO.findAll();
		Integer count = (Integer) employeeDAO.countById();
		EmployeeDAO emp = new EmployeeDAO();
		emp.setEmployees(employees);
		emp.setCount(count);
		System.out.println(emp);
		return new ResponseEntity<EmployeeDAO>(emp, HttpStatus.OK);
	}

	/* get employee by empid */
	@GetMapping("/employees/{id}")
	public ResponseEntity<Optional<Employee>> getEmployeeById(@PathVariable(value = "id") Long empid) {

		Optional<Employee> emp = employeeDAO.findById(empid);

		if (emp == null) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok().body(emp);
	}

	/* update an employee by empid */
	@RequestMapping(value = "/employees/{id}", method = RequestMethod.PUT)
//	@PutMapping("/employees/{id}")
	public ResponseEntity<Employee> updateEmployee(@PathVariable(value = "id") Long empid,
			@Validated Employee empDetails) {

		Optional<Employee> emp = employeeDAO.findById(empid);
		if (emp == null) {
			return ResponseEntity.notFound().build();
		} else {
//			System.out.print(empDetails.getName());
			empDetails.setName(empDetails.getName());
			empDetails.setDesignation(empDetails.getDesignation());
			empDetails.setExpertise(empDetails.getExpertise());
//			updateEmployee.setName(empDetails.getName());
			empDetails.setId(empid);
			Employee updateEmployee = employeeDAO.save(empDetails);

//			Employee updateEmployee=null;
			return ResponseEntity.ok().body(updateEmployee);
		}
	}

	/* Delete an employee */
	@DeleteMapping("/employees/{id}")
	public ResponseEntity<Optional<Employee>> deleteEmployee(@PathVariable(value = "id") Long empid) {

		Optional<Employee> emp = employeeDAO.findById(empid);

		if (emp == null) {
			return ResponseEntity.notFound().build();
		}
		employeeDAO.delete(empid);
//			EmployeeDAO employee = emp;
		emp.get().setMessage("deleted");
//			employee.setMessage()
		return ResponseEntity.ok().body(emp);

	}
}
